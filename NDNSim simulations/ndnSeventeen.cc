/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

// ndn-tree-tracers.cpp

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM/helper/ndn-link-control-helper.hpp"

namespace ns3 {

/**
 * This scenario simulates a tree topology (using topology reader module)
 *
 *    /------\      /------\      /------\      /------\      /------\
 *    |BITS-1|      |BITS-2|      |BITS-3|      |BITS-4|      |BITS-5|
 *    \------/      \------/      \------/      \------/      \------/
 *         ^          ^                ^           ^              ^
 *         |          |                |           |              |
 *          \        /                  \         /              /
 *           \      /                    \       /    __________/     10Mbps / 1ms
 *            \    /                      \    _/ ___/    
 *             |  |                        |  |  |           
 *             v  v                        v  v  v
 *          /-------\                     /-------\
 *          |  BM-1 |                     |  GP-1 |
 *          \-------/                     \-------/
 *                ^                         ^
 *                |                         |
 *                 \                        /  10 Mpbs / 1ms
 *                  +--------+   +-------- +
 *                           |   |
 *                           v   v
 *                        /--------\
 *                        |  PP-1  |
 *                        \--------/
 *
 *
 * To run scenario and see what is happening, use the following command:
 *
 *     ./waf --run=ndn-tree-tracers
 */

int
main(int argc, char* argv[])
{
  CommandLine cmd;
  cmd.Parse(argc, argv);

  AnnotatedTopologyReader topologyReader("", 1);
  topologyReader.SetFileName("scratch/topo-custom.txt");
  topologyReader.Read();

  // Install NDN stack on all nodes
  ndn::StackHelper ndnHelper;
  //ndnHelper.SetOldContentStore("ns3::ndn::cs::Lru","MaxSize","1000"); 
  //ndnHelper.SetOldContentStore("ns3::ndn::cs::Lfu","MaxSize","1000"); 
  //ndnHelper.SetOldContentStore("ns3::ndn::cs::Fifo","MaxSize","1000"); 
  ndnHelper.SetOldContentStore("ns3::ndn::cs::Random","MaxSize","1000"); // default ContentStore parameters
  ndnHelper.InstallAll();

  // Choosing forwarding strategy
  ndn::StrategyChoiceHelper::InstallAll("/prefix", "/localhost/nfd/strategy/broadcast");

  // Installing global routing interface on all nodes
  ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
  ndnGlobalRoutingHelper.InstallAll();

  // Getting containers for the consumer/producer
  Ptr<Node> consumers[5] = {Names::Find<Node>("BITS-1"), Names::Find<Node>("BITS-2"),
                            Names::Find<Node>("BITS-3"), Names::Find<Node>("BITS-4"),
                            Names::Find<Node>("BITS-5")};
  Ptr<Node> producer = Names::Find<Node>("PP-1");

  for (int i = 0; i < 5; i++) {
    ndn::AppHelper consumerHelper("ns3::ndn::ConsumerCbr");
    consumerHelper.SetAttribute("Frequency", StringValue("100")); // 100 interests a second
    if(i==0 || i==3 || i==4)
    consumerHelper.SetAttribute("StartTime", StringValue("5"));
    if(i==1)
    consumerHelper.SetAttribute("StartTime", StringValue("2"));

    consumerHelper.SetPrefix("/PP-1/");

    consumerHelper.Install(consumers[i]);
  }

  ndn::AppHelper producerHelper("ns3::ndn::Producer");
  producerHelper.SetAttribute("PayloadSize", StringValue("1024"));

  // Register /root prefix with global routing controller and
  // install producer that will satisfy Interests in /root namespace
  ndnGlobalRoutingHelper.AddOrigins("/PP-1", producer);
  producerHelper.SetPrefix("/PP-1");
  producerHelper.Install(producer);

  // Calculate and install FIBs
  ndn::GlobalRoutingHelper::CalculateRoutes();
  Simulator::Schedule(Seconds(10.0),ndn::LinkControlHelper::FailLink, Names::Find<Node>("PP-1"), Names::Find<Node>("BM-1"));
  Simulator::Schedule(Seconds(30.0),ndn::LinkControlHelper::UpLink, Names::Find<Node>("PP-1"), Names::Find<Node>("BM-1"));
  Simulator::Stop(Seconds(20.0));

//  ndn::L3RateTracer::InstallAll("rate-trace.txt", Seconds(0.5));
  ndn::CsTracer::InstallAll("cs-seventeen.txt",Seconds(1));

  Simulator::Run();
  Simulator::Destroy();

  return 0;
}

} // namespace ns3

int
main(int argc, char* argv[])
{
  return ns3::main(argc, argv);
}
