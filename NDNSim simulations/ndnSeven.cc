#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/ndnSIM-module.h"

namespace ns3 {

	int main(int argc, char* argv[])
	{
		Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("1Mbps"));
		Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
		Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("100"));

		CommandLine cmd;
		cmd.Parse(argc,argv);

		PointToPointHelper p2p;
		
		NodeContainer p2pNodes, p2pNodes2, p2pNodes3, csmaNodes, csmaNodes2;
		p2pNodes.Create(2);
		p2pNodes2.Create(2);
		p2pNodes3.Create(2);
		p2p.Install (p2pNodes);
		p2p.Install (p2pNodes2);
		p2p.Install (p2pNodes3);
		
		CsmaHelper csma;
		csma.SetChannelAttribute("DataRate", StringValue("1Mbps"));
		csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6500)));

		csmaNodes.Add(p2pNodes.Get(1));
		csmaNodes.Add(p2pNodes2.Get(1));
		csmaNodes.Add(p2pNodes3.Get(1));
		csmaNodes.Create(2);
		csma.Install(csmaNodes);

		csmaNodes2.Add(p2pNodes3.Get(0));
		csmaNodes2.Create(3);
		csma.Install(csmaNodes2);

		ndn::StackHelper ndnHelper;
		ndnHelper.InstallAll();

		ndn::StrategyChoiceHelper::InstallAll("/","/localhost/nfd/strategy/best-route");

		ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
		ndnGlobalRoutingHelper.InstallAll();

		Ptr<Node> producer = p2pNodes.Get(0);
		Ptr<Node> producer2 = p2pNodes2.Get(0);		
		NodeContainer consumerNodes, consumerNodes2;
		consumerNodes.Add(csmaNodes2.Get(2));
		consumerNodes2.Add(csmaNodes.Get(4));

		std::string prefix="/superawsm";
		std::string prefix2="/wonderful";
		
		ndn::AppHelper consumerHelper("ns3::ndn::ConsumerCbr");
		consumerHelper.SetPrefix(prefix);
		consumerHelper.SetAttribute("Frequency", StringValue("1"));//1 interest a second
		consumerHelper.Install(consumerNodes);

		ndn::AppHelper consumerHelper2("ns3::ndn::ConsumerCbr");
		consumerHelper2.SetPrefix(prefix2);
		consumerHelper2.SetAttribute("Frequency", StringValue("1"));//1 interest a second
	//	consumerHelper2.SetAttribute("StartTime", StringValue("1000ms"));
		consumerHelper2.Install(consumerNodes2);

		ndn::AppHelper producerHelper("ns3::ndn::Producer");
		producerHelper.SetPrefix(prefix2);
		producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
		producerHelper.Install(producer2);

		ndn::AppHelper producerHelper2("ns3::ndn::Producer");
		producerHelper2.SetPrefix(prefix);
		producerHelper2.SetAttribute("PayloadSize", StringValue("1024"));
//		producerHelper2.SetAttribute("StartTime", StringValue("1000ms"));		
		producerHelper2.Install(producer);

		ndnGlobalRoutingHelper.AddOrigins(prefix, producer);
		ndnGlobalRoutingHelper.AddOrigins(prefix2, producer2);
		
		ndn::GlobalRoutingHelper::CalculateRoutes();

		Simulator::Stop(Seconds(20.0));
		Simulator::Run();
		Simulator::Destroy();
				
		return 0;
	}
}

int main(int argc, char* argv[])
{
	return ns3::main(argc,argv);
}
