#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/stats-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ndnSIM/helper/ndn-link-control-helper.hpp"
#include<iostream>


namespace ns3 {
	void AdvancePosition (Ptr<Node> node);
	Vector GetPosition (Ptr<Node> node);
	void SetPosition (Ptr<Node> node, Vector position);
	uint32_t m_bytesTotal;
	Gnuplot2dDataset m_output;
	int main(int argc, char* argv[])
	{
		Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("5Mbps"));
		Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
		Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("100"));

		CommandLine cmd;
		cmd.Parse(argc,argv);

  		AnnotatedTopologyReader topologyReader("", 10);
		topologyReader.SetFileName("scratch/topo-wifi.txt");
  		topologyReader.Read();

		PointToPointHelper p2p;

		NodeContainer p2pNodes;
//		p2pNodes.Create(2);
		p2pNodes.Add(Names::Find<Node>("CSU-1"));
		p2pNodes.Add(Names::Find<Node>("UCLA-1"));
		p2p.Install(p2pNodes);

		NodeContainer wifiStaNodes,wifiStaNodes2;
  		wifiStaNodes.Create (1);
  		NodeContainer wifiApNode = p2pNodes.Get (0);
		NodeContainer wifiApNode2 = p2pNodes.Get (1);
		// wifi 1
		
		YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
		channel.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange",DoubleValue(100));
  		YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  		phy.SetChannel (channel.Create ());
		
  		WifiHelper wifi = WifiHelper::Default ();
  		wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

 		NqosWifiMacHelper mac = NqosWifiMacHelper::Default ();
		Ssid ssid = Ssid ("SUPERMAN");
  		mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (true));

		NetDeviceContainer staDevices;
  		staDevices = wifi.Install (phy, mac, wifiStaNodes);

  		mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  		NetDeviceContainer apDevices;
  		apDevices = wifi.Install (phy, mac, wifiApNode);

		MobilityHelper mobility,mobility2;

  		mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (-30.0),
                                 "MinY", DoubleValue (-10.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (0.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

  		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel"
		/*"ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-100, 100, -100, 100))*/);
  		mobility.Install (wifiStaNodes);

  		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  		mobility.Install (wifiApNode);
		

		// wifi 2 
		YansWifiChannelHelper channel2 = YansWifiChannelHelper::Default ();
  		YansWifiPhyHelper phy2 = YansWifiPhyHelper::Default ();
		phy2.SetChannel (channel2.Create ());

  		WifiHelper wifi2 = WifiHelper::Default ();
  		wifi2.SetRemoteStationManager ("ns3::AarfWifiManager");

 		NqosWifiMacHelper mac2 = NqosWifiMacHelper::Default ();
		Ssid ssid2 = Ssid ("SUPERMAN");
  		mac2.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid2));

  		NetDeviceContainer apDevices2;
//edit here
  		apDevices2 = wifi2.Install (phy, mac2, wifiApNode2);

		mobility2.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  		mobility2.Install (wifiApNode2);

		ndn::StackHelper ndnHelper;
		ndnHelper.InstallAll();

		ndn::StrategyChoiceHelper::InstallAll("/","/localhost/nfd/strategy/best-route");

		ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
		ndnGlobalRoutingHelper.InstallAll();

		Ptr<Node> producer = p2pNodes.Get(0);	
		NodeContainer consumerNodes;
		consumerNodes.Add(wifiStaNodes.Get(0));

		phy.EnablePcapAll("wifipcap");
		
		std::string prefix="/superawsm";
		
		ndn::AppHelper consumerHelper("ns3::ndn::ConsumerCbr");
		consumerHelper.SetPrefix(prefix);
		consumerHelper.SetAttribute("Frequency", StringValue("10"));//1 interest a second
		consumerHelper.Install(consumerNodes);

		ndn::AppHelper producerHelper("ns3::ndn::Producer");
		producerHelper.SetPrefix(prefix);
		producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
		producerHelper.Install(producer);

		ndnGlobalRoutingHelper.AddOrigins(prefix, producer);
		
		ndn::GlobalRoutingHelper::CalculateRoutes();

		Simulator::Stop(Seconds(20.0));
		Simulator::Schedule (Seconds (1.5), AdvancePosition, wifiStaNodes.Get (0));		
		Simulator::Run();
		Simulator::Destroy();
		
		

		return 0;
		}	
		void AdvancePosition (Ptr<Node> node) 
		{
 		 Vector pos = GetPosition (node);
 		 double mbs = ((m_bytesTotal * 8.0) / 1000000);
 		 m_bytesTotal = 0;
 		 m_output.Add (pos.x, mbs);
 		 pos.x += 1.0;
 		 if (pos.x >= 210.0) 
   		 {
   		   return;
   		 }
		 if(pos.x>=100)
		 {
					
		 }
 		 SetPosition (node, pos);
	 	 //std::cout << "x="<<pos.x << std::endl;
	 		 Simulator::Schedule (Seconds (1.0), AdvancePosition, node);
		}

		Vector GetPosition (Ptr<Node> node)
		{
 		 Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  		return mobility->GetPosition ();
		
		}
		void SetPosition (Ptr<Node> node, Vector position)
		{
  		Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  		mobility->SetPosition (position);
		}
		
}


int main(int argc, char* argv[])
{
	return ns3::main(argc,argv);
}
