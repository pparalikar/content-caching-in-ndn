#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"

namespace ns3 {

	int main(int argc, char* argv[])
	{
		Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("5Mbps"));
		Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
		Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("100"));

		CommandLine cmd;
		cmd.Parse(argc,argv);

		PointToPointHelper p2p;
		
		NodeContainer p2pNodes, p2pNodes2, p2pNodes3, csmaNodes, csmaNodes2;
		p2pNodes.Create(2);
		p2pNodes2.Create(2);
		p2pNodes3.Create(2);
		p2p.Install (p2pNodes);
		p2p.Install (p2pNodes2);
		p2p.Install (p2pNodes3);
		
		CsmaHelper csma;
		csma.SetChannelAttribute("DataRate", StringValue("1Mbps"));
		csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6500)));

		csmaNodes.Add(p2pNodes.Get(1));
		csmaNodes.Add(p2pNodes2.Get(1));
		csmaNodes.Add(p2pNodes3.Get(1));
		csmaNodes.Create(2);
		csma.Install(csmaNodes);

		csmaNodes2.Add(p2pNodes3.Get(0));
		csmaNodes2.Create(3);
		csma.Install(csmaNodes2);

  /*First Wifi*/
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (3);
  NodeContainer wifiApNode = p2pNodes.Get (0);

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

  NqosWifiMacHelper mac = NqosWifiMacHelper::Default ();

  Ssid ssid = Ssid ("SUPERMAN");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);

  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);

  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (-30.0),
                                 "MinY", DoubleValue (-10.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel" /* "ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-30, -10, 0, 10))*/);
  mobility.Install (wifiStaNodes);

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);

  /* Second Wifi */

  NodeContainer wifiStaNodes2;
  wifiStaNodes2.Create (3);
  NodeContainer wifiApNode2 = p2pNodes2.Get (0);

  YansWifiChannelHelper channel2 = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy2 = YansWifiPhyHelper::Default ();
  phy2.SetChannel (channel2.Create ());

  WifiHelper wifi2 = WifiHelper::Default ();
  wifi2.SetRemoteStationManager ("ns3::AarfWifiManager");

  NqosWifiMacHelper mac2 = NqosWifiMacHelper::Default ();

  Ssid ssid2 = Ssid ("BATMAN");
  mac2.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid2),
               "ActiveProbing", BooleanValue (false));

  NetDeviceContainer staDevices2;
  staDevices2 = wifi.Install (phy2, mac2, wifiStaNodes2);

  mac2.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid2));

  NetDeviceContainer apDevices2;
  apDevices2 = wifi2.Install (phy2, mac2, wifiApNode2);

  MobilityHelper mobility2;

  mobility2.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (20.0),
                                 "MinY", DoubleValue (-20.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility2.SetMobilityModel ("ns3::ConstantPositionMobilityModel" /* "ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Recta0gle (20, -20, 40, 10))*/);
  mobility2.Install (wifiStaNodes2);

  mobility2.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility2.Install (wifiApNode2);


		ndn::StackHelper ndnHelper;
		ndnHelper.InstallAll();

		ndn::StrategyChoiceHelper::InstallAll("/","/localhost/nfd/strategy/best-route");

		ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
		ndnGlobalRoutingHelper.InstallAll();

		Ptr<Node> producer = wifiStaNodes2.Get(1);
		NodeContainer consumerNodes;
		consumerNodes.Add(csmaNodes2.Get(2));

		std::string prefix="/superawsm";
		
		ndn::AppHelper consumerHelper("ns3::ndn::ConsumerCbr");
		consumerHelper.SetPrefix(prefix);
		consumerHelper.SetAttribute("Frequency", StringValue("1"));//1 interest a second
		consumerHelper.Install(consumerNodes);

		ndn::AppHelper producerHelper("ns3::ndn::Producer");
		producerHelper.SetPrefix(prefix);
		producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
		producerHelper.Install(producer);

		ndnGlobalRoutingHelper.AddOrigins(prefix, producer);
		
		ndn::GlobalRoutingHelper::CalculateRoutes();

		Simulator::Stop(Seconds(20.0));
		Simulator::Run();
		Simulator::Destroy();
				
		return 0;
	}
}

int main(int argc, char* argv[])
{
	return ns3::main(argc,argv);
}
