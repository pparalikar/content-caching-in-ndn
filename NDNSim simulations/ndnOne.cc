#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/ndnSIM-module.h"

namespace ns3 {

	int main(int argc, char* argv[])
	{
		Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("1Mbps"));
		Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
		Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("10"));

		CommandLine cmd;
		cmd.Parse(argc,argv);

		PointToPointHelper p2p;
		PointToPointGridHelper grid(3, 3, p2p);
		grid.BoundingBox(100,100,200,200);
		
		ndn::StackHelper ndnHelper;
		ndnHelper.InstallAll();

		ndn::StrategyChoiceHelper::InstallAll("/","/localhost/nfd/strategy/best-route");

		ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
		ndnGlobalRoutingHelper.InstallAll();

		Ptr<Node> producer = grid.GetNode(2,0);
		NodeContainer consumerNodes;
		consumerNodes.Add(grid.GetNode(0,2));

		std::string prefix="/paddy";
		
		ndn::AppHelper consumerHelper("ns3::ndn::ConsumerCbr");
		consumerHelper.SetPrefix(prefix);
		consumerHelper.SetAttribute("Frequency", StringValue("0.1"));//1 interest a second
		consumerHelper.Install(consumerNodes);
	
		ndn::AppHelper producerHelper("ns3::ndn::Producer");
		producerHelper.SetPrefix(prefix);
		producerHelper.SetAttribute("PayloadSize", StringValue("512"));//1 interest a second
		producerHelper.Install(producer);

		ndnGlobalRoutingHelper.AddOrigins(prefix, producer);
		
		ndn::GlobalRoutingHelper::CalculateRoutes();

		Simulator::Stop(Seconds(20.0));
		Simulator::Run();
		Simulator::Destroy();
				
		return 0;
	}
}

int main(int argc, char* argv[])
{
	return ns3::main(argc,argv);
}
