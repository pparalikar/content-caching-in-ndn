#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/ndnSIM-module.h"

namespace ns3 {

	int main(int argc, char* argv[])
	{
		Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("10kbps"));
		Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
		Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("10"));

		CommandLine cmd;
		cmd.Parse(argc,argv);

		PointToPointHelper p2p;
		//PointToPointGridHelper grid(3, 3, p2p);
		//grid.BoundingBox(100,100,200,200);
		
		NodeContainer p2pNodes, p2pNodes2, csmaNodes;
		p2pNodes.Create(2);
		p2pNodes2.Create(2);
		NetDeviceContainer p2pDev = p2p.Install (p2pNodes);
		NetDeviceContainer p2pDev2 = p2p.Install (p2pNodes2);
		
		CsmaHelper csma;
		csma.SetChannelAttribute("DataRate", StringValue("10kbps"));
		csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6500)));
		csmaNodes.Add(p2pNodes.Get(1));
		csmaNodes.Add(p2pNodes2.Get(1));
		csmaNodes.Create(3);
		NetDeviceContainer csmaDev = csma.Install(csmaNodes);

		ndn::StackHelper ndnHelper;
		ndnHelper.InstallAll();

		ndn::StrategyChoiceHelper::InstallAll("/","/localhost/nfd/strategy/best-route");

		ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
		ndnGlobalRoutingHelper.InstallAll();

		Ptr<Node> producer = p2pNodes.Get(0);
		NodeContainer consumerNodes, consumerNodes2;
		consumerNodes.Add(p2pNodes2.Get(0));
		//consumerNodes.Add(grid.GetNode(0,1));
		//consumerNodes.Add(grid.GetNode(1,0));
		//consumerNodes2.Add(grid.GetNode(2,2));
		//consumerNodes2.Add(grid.GetNode(1,2));

		std::string prefix="/paddy";
		
		ndn::AppHelper consumerHelper("ns3::ndn::ConsumerCbr");
		consumerHelper.SetPrefix(prefix);
		consumerHelper.SetAttribute("Frequency", StringValue("1"));//1 interest a second
//		consumerHelper.SetAttribute("StartTime", StringValue("1000ms"));
//		consumerHelper.SetInterestLifetime("10000ms");
		consumerHelper.Install(consumerNodes);

		//ndn::AppHelper consumerHelper2("ns3::ndn::ConsumerCbr");
		//consumerHelper2.SetPrefix(prefix);
		//consumerHelper2.SetAttribute("Frequency", StringValue("1"));//1 interest a second
		//consumerHelper2.Install(consumerNodes2);
	
		ndn::AppHelper producerHelper("ns3::ndn::Producer");
		producerHelper.SetPrefix(prefix);
//		producerHelper.SetAttribute("Freshness", StringValue("0"));
		producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
//		producerHelper.SetAttribute("StartTime", StringValue("10000ms"));
		producerHelper.Install(producer);

		ndnGlobalRoutingHelper.AddOrigins(prefix, producer);
		
		ndn::GlobalRoutingHelper::CalculateRoutes();

		Simulator::Stop(Seconds(20.0));
		Simulator::Run();
		Simulator::Destroy();
				
		return 0;
	}
}

int main(int argc, char* argv[])
{
	return ns3::main(argc,argv);
}
