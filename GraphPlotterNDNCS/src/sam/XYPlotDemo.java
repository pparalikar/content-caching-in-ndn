package sam;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class XYPlotDemo extends ApplicationFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XYPlotDemo(String title, String chartTitle) {
		super(title);
		JFreeChart xylineChart = ChartFactory.createXYLineChart(chartTitle,"Time","Cache Hits",createDataset(),PlotOrientation.VERTICAL,true,true,false);                
		ChartPanel chartPanel = new	ChartPanel( xylineChart );
				chartPanel.setPreferredSize( 
				new
				java.awt.Dimension( 560 , 367 ) );
				final
				XYPlot plot = xylineChart.getXYPlot( );
				XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
				renderer.setSeriesPaint( 0 , Color.RED);
				renderer.setSeriesPaint( 1 , Color.GREEN
				);
				renderer.setSeriesPaint( 2 , Color.
				YELLOW
				);
				renderer.setSeriesStroke( 0 , 
				new
				BasicStroke( 4.0f ) );
				renderer.
				setSeriesStroke( 1 , 
				new
				BasicStroke( 3.0f ) );
				renderer.setSeriesStroke( 2 , 
				new
				BasicStroke( 2.0f ) );
				plot.setRenderer( renderer );
				setContentPane( chartPanel );
	}
		
	private XYDataset createDataset( ) 
	{
		XYSeriesCollection dataset = new	XYSeriesCollection( );
		XYSeries hits = new XYSeries("CacheHits");
		XYSeries miss = new XYSeries("CacheMisses");
	
	try{
	BufferedReader br = new BufferedReader(new FileReader(new File("/home/bupean/ndnSIM/ns-3/cs-seventeen.txt").getAbsoluteFile()));
	String current=br.readLine();
	System.out.println(current);
	while((current=br.readLine())!=null)
	{
		StringTokenizer st = new StringTokenizer(current);
		String xval=st.nextToken();
		if(!st.nextToken().equalsIgnoreCase("BM-1"))
		{
			continue;
		}
		if(!st.nextToken().equalsIgnoreCase("CacheMisses"))
		{
			String yval=st.nextToken();
			hits.add(Double.valueOf(xval),Double.valueOf(yval));
		}
		else
		{
			String yval=st.nextToken();
			
			miss.add(Double.valueOf(xval),Double.valueOf(yval));
		}
		
		
	}
	dataset.addSeries(hits);
	dataset.addSeries(miss);
	br.close();
	}
	catch(Exception e)
	{
		System.out.println("Lochaaa"+e);
	}
	return	dataset;
	}
	public static void	main( String[ ] args )
	{
	XYPlotDemo chart = new	XYPlotDemo("Cache Hits/Miss" , "Cache Hits vs Time");
	chart.pack( );
	RefineryUtilities.centerFrameOnScreen( chart );
	chart.setVisible( true	);     
	}
	
}
