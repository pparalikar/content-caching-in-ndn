package sam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class LineChartDemo extends ApplicationFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LineChartDemo(String title, String chartTitle) {
		super(title);
		JFreeChart lineChart = ChartFactory.createLineChart(chartTitle,"Time","Cache Hits",createDataset(),PlotOrientation.VERTICAL,true,true,false);                
		ChartPanel chartPanel = new ChartPanel( lineChart );
		chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );
		setContentPane(chartPanel);
	}
		
	private DefaultCategoryDataset createDataset( ) 
	{
	DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
	try{
	BufferedReader br = new BufferedReader(new FileReader(new File("/home/bupean/ndnSIM/ns-3/cs-seventeen.txt").getAbsoluteFile()));
	String current=br.readLine();
	System.out.println(current);
	while((current=br.readLine())!=null)
	{
		StringTokenizer st = new StringTokenizer(current);
		String xval=st.nextToken();
		if(!st.nextToken().equalsIgnoreCase("BM-1"))
		{
			continue;
		}
		if(!st.nextToken().equalsIgnoreCase("CacheMisses"))
		{
			continue;
		}
		String yval=st.nextToken();
		System.out.println(xval+" "+yval);
		dataset.addValue(Double.valueOf(yval),"Hits",xval);
	}
	br.close();
	}
	catch(Exception e)
	{
		System.out.println("Lochaaa"+e);
	}
	return	dataset;
	}
	public static void	main( String[ ] args )
	{
	LineChartDemo chart = new	LineChartDemo("Cache Hits" , "Cache Hits vs Time");
	chart.pack( );
	RefineryUtilities.centerFrameOnScreen( chart );
	chart.setVisible( true	);     
	}
	
}
