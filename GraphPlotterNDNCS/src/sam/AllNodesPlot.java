package sam;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class AllNodesPlot extends ApplicationFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AllNodesPlot(String title, String chartTitle) {
		super(title);
		JFreeChart xylineChart = ChartFactory.createXYLineChart(chartTitle,"Time","Cache Ratio",createDataset(),PlotOrientation.VERTICAL,true,true,false);                
		ChartPanel chartPanel = new	ChartPanel( xylineChart );
				chartPanel.setPreferredSize( 
				new
				java.awt.Dimension( 1000 , 750 ) );
				final
				XYPlot plot = xylineChart.getXYPlot( );
				XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
				renderer.setSeriesPaint( 0 , Color.RED);
				renderer.setSeriesPaint( 1 , Color.GREEN);
				renderer.setSeriesPaint( 2 , Color.BLUE);
				renderer.setSeriesStroke( 0 , new BasicStroke( 4.0f ) );
				renderer.setSeriesStroke( 1 , new BasicStroke( 4.0f ) );
				renderer.setSeriesStroke( 2 , new BasicStroke( 4.0f ) );
				plot.setRenderer( renderer );
				setContentPane( chartPanel );
	}
		
	private XYDataset createDataset( ) 
	{
		XYSeriesCollection dataset = new	XYSeriesCollection( );
		XYSeries BM = new XYSeries("BM-1");
		XYSeries GP = new XYSeries("GP-1");
		XYSeries PP = new XYSeries("PP-1");
	
	try{
	BufferedReader br = new BufferedReader(new FileReader(new File("/home/bupean/ndnSIM/ns-3/cs-seventeen.txt").getAbsoluteFile()));
	String current=br.readLine();
	System.out.println(current);
	while((current=br.readLine())!=null)
	{
		StringTokenizer st = new StringTokenizer(current);
		String xval=st.nextToken();
		String tok = st.nextToken();
		System.out.println(xval+tok);
		st.nextToken();
		String yval = "";
		if(!tok.equalsIgnoreCase("BM-1") && !tok.equalsIgnoreCase("GP-1") && !tok.equalsIgnoreCase("PP-1")) continue;
		yval = st.nextToken();
		current = br.readLine();

		st = new StringTokenizer(current);
		st.nextToken();
		st.nextToken();
		st.nextToken();
		Double val = Double.valueOf(yval);
		val = val/(val+Double.valueOf(st.nextToken()));
		System.out.println(val);
		if(tok.equalsIgnoreCase("BM-1"))
		{
			BM.add(Double.valueOf(xval),val);
		}
		else if(tok.equalsIgnoreCase("GP-1"))
		{
			GP.add(Double.valueOf(xval),val);
		}
		else{
			PP.add(Double.valueOf(xval),val);;
		}
	}
	dataset.addSeries(BM);
	dataset.addSeries(GP);
	dataset.addSeries(PP);
	br.close();
	}
	catch(Exception e)
	{
		System.out.println("Lochaaa"+e);
	}
	return	dataset;
	}
	public static void	main( String[ ] args )
	{
		AllNodesPlot chart = new AllNodesPlot("Cache Ratio for nodes" , "Cache Ratio vs Time");
	chart.pack( );
	RefineryUtilities.centerFrameOnScreen( chart );
	chart.setVisible( true	);     
	}
	
}
